﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HlMath.Matrix
{
     public  class Matrix:ICloneable
    {
        /// <summary>
        /// 2-d array value of the matrix.
        /// </summary>
        public double [,] Value { get;set;}

        /// <summary>
        /// Definition row of the matrix.
        /// </summary>
        public int Row { get; set; }

        /// <summary>
        /// Definition column of the matrix.
        /// </summary>
        public int Column { get; set; }

        /// <summary>
        /// initialize a matrix given row and column
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        public Matrix(int row,int column):this (new double [row ,column])
        {

        }
        /// <summary>
        /// initialize a Matrix 
        /// </summary>
        /// <param name="value"></param>
        public Matrix (double [,] value)
        {
            Row = value.GetLength(0);
            Column = value.GetLength(1);
            this.Value = value.Clone() as double[,];
        }
        /// <summary>
        /// Definition order matrix.
        /// </summary>
        /// <param name="order"></param>
        public Matrix(int order):this(order,order)
        {
            
        }
        /// <summary>
        /// initialize another matrix.
        /// </summary>
        /// <param name="x"></param>
        public Matrix(Matrix x):this(x.Value)
        {

        }
        /// <summary>
        /// clone
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return new Matrix(this.Value);
        }
        /// <summary>
        /// override a matrix in string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            for (var i = 0; i < this.Row; i++)
            {
                for (var j = 0; j < this.Column; j++)
                {
                    sb.Append(this.Value[i, j] + " ");
                }
                sb.Append('\n');
            }
            return sb.ToString();
        }
        /// <summary>
        /// addition
        /// </summary>
        /// <param name="m1"></param>
        /// <param name="m2"></param>
        /// <returns></returns>
        public static Matrix operator +(Matrix m1, Matrix m2)
        {
            var m = new Matrix(m1.Row, m1.Column);
            for (var i = 0; i < m1.Row; i++)
            {
                for (var j = 0; j < m1.Column; j++)
                {
                    m.Value[i, j] = m1.Value[i, j] + m2.Value[i, j];
                }
            }
            return m;
        }
        /// <summary>
        /// subtraction
        /// </summary>
        /// <param name="m1"></param>
        /// <param name="m2"></param>
        /// <returns></returns>
        public static Matrix operator -(Matrix n1, Matrix n2)
        {
            var n = new Matrix(n1.Row, n1.Column);
            for (var i = 0; i < n1.Row; i++)
            {
                for (var j = 0; j < n1.Column; j++)
                {
                    n.Value[i, j] = n1.Value[i, j] - n2.Value[i, j];
                }
            }
            return n;
        }
        /// <summary>
        /// multiplication
        /// </summary>
        /// <param name="o1"></param>
        /// <param name="o2"></param>
        /// <returns></returns>
        public static Matrix operator * (Matrix o1,Matrix o2)
        {
            var o = new Matrix(o1.Row, o1.Column);
            for (var i = 0; i < o1.Row; i++)
            {
                for (var j = 0; j < o2.Column; j++)
                {
                    for (int k = 0; k <o1.Column ; k++)
                    {
                        o.Value[i, j] += o1.Value[i, k] * o2.Value[k, j]; 
                    }
                }
            }
            return o;
        }
      public Matrix  Transpose()
        {
            var p = new Matrix(Row,Column);
            for (int i = 0; i < Column; i++)
            {
                for (int j = 0; j <Row; j++)
                {
                    p.Value[i, j] = Value[j, i];
                }
            }
            return p ;
        }

    }
}
