﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HlMath.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please input the row of a Matrix：");
            var row = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Please input the column of this Matrix");
            var column = Convert.ToInt32(Console.ReadLine());
            var matrix = new Matrix.Matrix(row, column);
            for (var i = 0; i < row; i++)
            {
                for (var j = 0; j < column; j++)
                {
                    Console.WriteLine($"Please input {i},{j} element value：");
                    matrix.Value[i, j] = Convert.ToDouble(Console.ReadLine());
                }
            }
            Console.WriteLine(matrix.ToString());

            var m2 = matrix.Clone() as Matrix.Matrix;
            var m3 = matrix + m2;
            Console.WriteLine(m3);

            var n2= matrix.Clone() as Matrix.Matrix;
            var n3 = matrix - n2;
            Console.WriteLine(n3);

            var o2 = matrix.Clone() as Matrix.Matrix;
            var o3 = matrix * o2;
            Console.WriteLine(o3);

            var p = matrix.Transpose();
            Console.WriteLine(p);
            Console.ReadLine();
        }
    }
}
